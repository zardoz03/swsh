#!/bin/sh
set -e
clear
printf "Use another shell other than bash? (y/n)\n:: > "
read -r use_other_shell
[ "$use_other_shell" = "y" ] || exit

printf "what shell? \n|  scsh\n|  rash\n:: > "
read -r use_shell

[ -z "$use_shell" ] && { exit ;} || {
  case "$use_shell" in
    rash|rkt) exec rash && exit ;;
    scsh|scm) exec scsh && exit ;;
    .*) exit ;;
  esac
}
